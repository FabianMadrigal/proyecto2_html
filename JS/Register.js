
const form = document.getElementById('form');
const name = document.getElementById('name');
const lastname = document.getElementById('lastname')
const phone = document.getElementById('phone')
const username = document.getElementById('user');
const password = document.getElementById('password');
const cPassword = document.getElementById('conf-password')

form.addEventListener('submit', function (event) {
    event.preventDefault();
    let users = Array(
        {
            name: name.value,
            lastname: lastname.value,
            phone: phone.value,
            username: username.value,
            password: password.value,
            cPassword: cPassword.value
        }
    );
    localStorage.setItem('users', JSON.stringify(users));
        location.href= 'index.html';
});

