document.querySelector('#btnprovincia').addEventListener('click',function(){
    obtProvinvia();
});

document.querySelector('#btncanton').addEventListener('click',function(){
    obtCanton();
});

document.querySelector('#to').addEventListener('click',function(){
    obtDistrito();
});

function obtProvinvia(){
    var webProvincia='https://ubicaciones.paginasweb.cr/provincias.json';
    traerDatos(webProvincia,'provincia');    
}

function obtCanton(){
    var idProvincia=document.getElementById('provincia').value;
    var webCanton='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/cantones.json'

    traerDatos(webCanton,'canton');    
}

function obtDistrito(){
    var idProv=document.getElementById('alajuela').value;
    var idCant=document.getElementById('San Carlos').value;
    //Armar el url con las variables necesarias para completar
    var webDistrito='https://ubicaciones.paginasweb.cr/provincia/'
    +idProv+'/canton/'+idCant+'/distritos.json'
    traerDatos(webDistrito,'to');    
}

function obtDistrito(){
    var idProv=document.getElementById('provincia').value;
    var idCant=document.getElementById('canton').value;
    //Armar el url con las variables necesarias para completar
    var webDistrito='https://ubicaciones.paginasweb.cr/provincia/'
    +idProv+'/canton/'+idCant+'/distritos.json'
    traerDatos(webDistrito,'from');    
}


function traerDatos(purl,ptag){
    fetch(purl)
    .then((respuesta)=>{
        return respuesta.json();
    }).then((respuesta)=>{
        Limpiar(ptag);
        //declaro variable resultado en js
        let resultado= document.querySelector('#'+ptag);
            for (var clave in respuesta){
                if (respuesta.hasOwnProperty(clave)) {
                    resultado.innerHTML+="<option value='"+clave+"'>"
                    +respuesta[clave]+"</option>";
                        
                }
            }

    })
}

function Limpiar(tag)
{
    document.getElementById(tag).innerHTML = "";
}

